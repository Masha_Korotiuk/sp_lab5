#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "./layer2.h"
#include "./layer3.h"

int layer3_transmit(uint8_t buffer[], size_t bufferSize, bool lastPackage)
{
  puts("layer3 transmit");
  L3_TB_BLOCK *l3_tb = (L3_TB_BLOCK *)calloc(L3_TB_SIZE, sizeof(L2_TB_BLOCK));
  int num_blocks = bufferSize / L3_MAX_SEG_SIZE;
  for (int i = 0; i < num_blocks; ++i)
  {
    memcpy(&l3_tb[i].data, &buffer[i * L3_MAX_SEG_SIZE], L3_MAX_SEG_SIZE);
  }
  size_t last_block_size = bufferSize - num_blocks * L3_MAX_SEG_SIZE;
  if (last_block_size != 0)
  {
    memcpy(&l3_tb[num_blocks].data, &buffer[num_blocks * L3_MAX_SEG_SIZE], last_block_size);
    num_blocks++;
  }
  int res = layer2_transmit(l3_tb, num_blocks, last_block_size, lastPackage);
  free(l3_tb);
  return res;
}

int layer3_receive(uint8_t buffer[], bool *lastPackage)
{
  puts("layer3 receive");
  L3_TB_BLOCK *buffer1 = calloc(L3_TB_SIZE, sizeof(L2_TB_BLOCK));
  if (buffer1 == NULL)
  {
    return -1;
  }
  size_t lastBlockSize;
  size_t length = layer2_receive(buffer1, &lastBlockSize, lastPackage);
  for (size_t i = 0; i < length - 1; i++)
  {
    memcpy(&buffer[i * L3_MAX_SEG_SIZE], &buffer1[i].data, L3_MAX_SEG_SIZE);
  }
  memcpy(&buffer[(length - 1) * L3_MAX_SEG_SIZE], &buffer1[length - 1].data, lastBlockSize);
  free(buffer1);
  return L3_MAX_SEG_SIZE * (length - 1) + lastBlockSize;
}