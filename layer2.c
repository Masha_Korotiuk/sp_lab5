#include <stdint.h>
#include <string.h>
#include <sys/types.h>

#include "./layer1.h"
#include "./layer2.h"
#include "./gen_crc16.h"

int layer2_transmit(L3_TB_BLOCK l3_tb[], size_t bufferSize, size_t lastBlockSize, bool lastPackage)
{
  puts("layer2 transmit");
  L2_TB_BLOCK *l2_tb = (L2_TB_BLOCK *)l3_tb;
  for (ssize_t i = bufferSize - 1; i >= 0; i--)
  {
    L2_TB_BLOCK *package = &l2_tb[i];
    memmove(&package->l3_block, &l3_tb[i], sizeof(L3_TB_BLOCK));
    package->stx = 0x02;
    package->count[0] = 0;
    package->count[1] = 0;
    package->count[2] = i == bufferSize - 1 ? lastBlockSize : L3_MAX_SEG_SIZE;
    package->seq = i;
    package->ack = 0;
    if (i == bufferSize - 1)
    {
      if (lastPackage)
      {
        package->lframe = 0x0F;
      }
      else
      {
        package->lframe = 0x01;
      }
    }
    else
    {
      package->lframe = 0;
    }
    package->chksum = 0;
    package->etx = 0x03;
    package->chksum = gen_crc16((uint8_t *)package, sizeof(L2_TB_BLOCK));
  }
  for (int i = 0; i < bufferSize; i++)
  {
    int errors = 0;
    while (layer1_transmit(&l2_tb[i]) < 0)
    {
      errors += 1;
      if (errors >= TTR)
      {
        return -1;
      }
    }
  }
  return 0;
}
int layer2_receive(L3_TB_BLOCK l3_tb[], size_t *lastBlockSize, bool *lastPackage)
{
  puts("layer2 receive");
  L2_TB_BLOCK *l2_tb = (L2_TB_BLOCK *)l3_tb;
  for (size_t i = 0; i < L3_TB_SIZE; ++i)
  {
    l2_tb[i].seq = -1;
  }
  *lastBlockSize = sizeof(L3_TB_BLOCK);
  *lastPackage = false;
  size_t received;
  size_t waiting_for = L3_TB_SIZE;
  for (received = 0; received < waiting_for;)
  {
    L2_TB_BLOCK package = {0};
    if (layer1_receive(&package) < 0)
    {
      continue;
    }
    int8_t seq = package.seq;
    if (l2_tb[seq].seq == -1)
    {
      memcpy(&l2_tb[seq], &package, sizeof(L2_TB_BLOCK));
      received += 1;
    }
    if (package.lframe == 0x01 || package.lframe == 0x0F)
    {
      waiting_for = seq;
      *lastBlockSize = package.count[2];
      *lastPackage = package.lframe == 0x0F;
    }
  }
  for (size_t i = 0; i < received; i++)
  {
    memmove(&l3_tb[i], &l2_tb[i].l3_block, sizeof(L3_TB_BLOCK));
  }
  return received;
}