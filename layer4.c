#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "./layer3.h"
#include "./layer4.h"

int layer4_transmit(const char *filename)
{
  puts("layer4 transmit");
  FILE *fin = fopen(filename, "r");
  if (fin == NULL)
  {
    perror("fopen: ");
  }
  size_t bufferLength = L3_TB_SIZE * L3_MAX_SEG_SIZE;
  uint8_t buffer[bufferLength];
  size_t size;
  size_t sent = 0;
  while ((size = fread(buffer, 1, bufferLength, fin)) != 0)
  {
    bool is_eof = true;
    char c = fgetc(fin);
    if (feof(fin))
    {
      is_eof = true;
    }
    else
    {
      ungetc(c, fin);
      is_eof = false;
    }
    layer3_transmit(buffer, size, is_eof);
    sent += 1;
  }
  return 0;
}

int layer4_receive(const char *filename)
{
  puts("layer4 receive");
  FILE *out = fopen(filename, "w");
  size_t bufferLength = L3_TB_SIZE * L3_MAX_SEG_SIZE;
  uint8_t buffer[bufferLength];
  bool lastPackage = false;
  while (!lastPackage)
  {
    size_t length = layer3_receive(buffer, &lastPackage);
    size_t wroteBytes = fwrite(buffer, 1, length, out);
    if (wroteBytes == 0)
    {
      return -1;
    }
  }
  return 0;
}